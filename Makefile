#!/usr/bin/make -f

GOSRC := $(shell go list -f '{{join .GoFiles " "}}' ./...)
GODATA := $(wildcard $(shell go list -f '{{join .EmbedPatterns " "}}' ./...))
GOTESTSRC := $(shell go list -f '{{join .TestGoFiles " "}}' ./...)
GOTESTDATA := $(wildcard $(shell go list -f '{{join .TestEmbedPatterns " "}}' ./...))

GOCOVERAGE ?= go.coverage

all:

lint:
	golangci-lint run -E gofmt .

fmt:
	go mod tidy
	gofmt -w -s $(GOSRC) $(GOTESTSRC)

test: GOTESTFLAGS ?= -race
test:
	go test $(GOTESTFLAGS) ./...

clean:
	$(RM) $(GOCOVERAGE).html $(GOCOVERAGE).out

$(GOCOVERAGE).out: GOTESTCOVERFLAGS ?= -coverpkg=./... -covermode=atomic -coverprofile=$@
$(GOCOVERAGE).out: $(GOSRC) $(GODATA) $(GOTESTSRC) $(GOTESTDATA) go.mod
	$(MAKE) test GOTESTFLAGS="$(GOTESTFLAGS) $(GOTESTCOVERFLAGS)"

$(GOCOVERAGE).html: $(GOCOVERAGE).out
	go tool cover -html $< -o $@

$(GOCOVERAGE): $(GOCOVERAGE).out
	go tool cover -html $<


.PHONY: all test lint fmt
.DELETE_ON_ERROR:
