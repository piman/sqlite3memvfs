# `sqlite3memvfs`

[![Go
Reference](https://pkg.go.dev/badge/codeberg.org/piman/sqlite3memvfs.svg)](https://pkg.go.dev/codeberg.org/piman/sqlite3memvfs)

`codeberg.org/piman/sqlite3memvfs` implements support for reading data
from in-memory SQLite database files. This can be helpful if you want
to…

- … distribute an SQLite database with your program using [`embed`][].
- … load a non-local DB in a situation where memory isn’t more precious
  than disk storage (e.g. many containers).
- … do other things I didn’t think of.

This is based on
- [github.com/mattn/go-sqlite3][] for a SQLite driver;
- [github.com/psanford/sqlite3vfs][] for mapping the VFS layer.

If this sounds interesting, but your database outstrips your memory, but
you can keep an immutable copy on an external object store,
[github.com/psanford/sqlite3vfshttp][] may be more appropriate.

[`embed`]: https://pkg.go.dev/embed
[github.com/mattn/go-sqlite3]: https://github.com/mattn/go-sqlite3
[github.com/psanford/sqlite3vfs]: https://github.com/psanford/sqlite3vfs
[github.com/psanford/sqlite3vfshttp]: https://github.com/psanford/sqlite3vfshttp

## License

Copyright 2022 Joe Wreschnig

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either [version 3 of the License](COPYING), or
(at your option) any later version.
