package sqlite3memvfs

import (
	"errors"
	"io"
)

// An Entry implements the features needed for an immutable SQLite3
// file; that is, reading from a specified offset and having a size.
//
// If implementing your own Entry see the parallel use requirements of
// [io.ReaderAt.ReadAt]. Also note the lack of Close; a VFS may stop
// referencing an Entry but will never do anything to dispose of one.
type Entry interface {
	io.ReaderAt
	Size() int64
}

var errNeg = errors.New("entry.ReadAt: negative offset")

type entry[T ~[]byte | ~string] struct{ data T }

func (r entry[T]) ReadAt(b []byte, off int64) (n int, err error) {
	switch {
	case off < 0:
		return 0, errNeg
	case off >= int64(len(r.data)):
		return 0, io.EOF
	default:
		if n = copy(b, r.data[off:]); n < len(b) {
			err = io.EOF
		}
		return
	}
}

func (r entry[T]) Size() int64 { return int64(len(r.data)) }
