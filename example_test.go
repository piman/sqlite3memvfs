package sqlite3memvfs_test

import (
	"database/sql"
	_ "embed"
	"fmt"

	"codeberg.org/piman/sqlite3memvfs"
)

//go:embed testdata/test.db
var dbfile string

func init() {
	sqlite3memvfs.Must(
		sqlite3memvfs.RegisteredAs("example"),
		sqlite3memvfs.WithEntryData("test.db", dbfile),
	)
}

func Example_embed() {
	db, _ := sql.Open("sqlite3", "test.db?vfs=example")
	defer db.Close()
	row := db.QueryRow("select value from test where id = 1")

	var result string
	if row.Scan(&result) == nil {
		fmt.Println(result)
	}
	// Output: first row
}
