package sqlite3memvfs

import (
	"github.com/psanford/sqlite3vfs"
)

// An Option can be passed to [Must] or [New] to prepare a VFS during
// its creation.
type Option func(*VFS) error

// WithEntryData creates the VFS with an entry containing the provided
// data.
//
// Prefer string data when it’s embedded at compile-time (e.g. via
// [embed]). Prefer bytes when you already have immutable bytes, which
// is usual when you generate or load data at runtime.
func WithEntryData[T ~[]byte | ~string](name string, data T) Option {
	return WithEntry(name, entry[T]{data})
}

// WithEntry creates the VFS with the provided entry.
func WithEntry(name string, r Entry) Option {
	return func(v *VFS) error {
		return v.Entry(name, r)
	}
}

// RegisteredAs will register this VFS with the provided name and
// options during creation. (If you want to do this later instead, use
// [sqlite3vfs.RegisterVFS].)
//
// It’s not possible to unregister a VFS, so registering a VFS once at
// startup and adding/removing entries is recommended if you need to
// handle changing datasets over time.
func RegisteredAs(name string, opts ...sqlite3vfs.Option) Option {
	return func(v *VFS) error {
		return sqlite3vfs.RegisterVFS(name, v, opts...)
	}
}
