// Package sqlite3memvfs implements support for reading data from
// in-memory SQLite database files, keyed in a [VFS] which can be
// accessed via [database/sql.Open] using the “vfs” query parameter.
// Multiple VFSs may be registered under different names; “files” may
// also be created or removed from each VFS independently.
//
// This is subtly different than the [:memory:] filename which is used
// to create an in-memory database scoped to a single connection. This
// package is for when you already have static data and don’t want to
// use a real filesystem at all.
//
// This uses the API provided by [sqlite3vfs] and the SQLite [OS
// interface], but you don’t need to know the details of either to use
// this.
//
// [:memory:]: https://www.sqlite.org/inmemorydb.html
// [OS interface]: https://www.sqlite.org/vfs.html
package sqlite3memvfs

import (
	"sync"

	"github.com/psanford/sqlite3vfs"
)

// New returns a new in-memory virtual filesystem for SQLite. Each VFS’s
// contents can be managed independently, and registered under a
// different SQLite VFS name.
//
// As the data is static and VFSs cannot be unregistered, any errors
// during creation are probably unrecoverable. Panicking on error is
// recommended; see [Must].
func New(opts ...Option) (*VFS, error) {
	v := new(VFS)
	for _, opt := range opts {
		if err := opt(v); err != nil {
			return nil, err
		}
	}
	return v, nil
}

// Must is like New but panics on errors.
func Must(opts ...Option) *VFS {
	vfs, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return vfs
}

// A VFS is a SQLite virtual filesystem with some particular contents.
//
// The data in the VFS should be treated immutably, both within and
// externally to SQLite operations. Attempting to modify the contents
// via SQLite will fail with an error. Modifying the VFS directly comes
// with similar caveats as an on-disk filesystem:
//
//   - Changing entries not used by any open [database/sql.Conn] is
//     fine.
//
//   - Replacing an entry used by an open connection may be fine but
//     different connections will end up with different views of the
//     “same” database. This may also cause more subtle problems if
//     you’re using [shared-cache mode].
//
//   - Changing the underlying contents of an entry (e.g. the original
//     []byte passed) while a connection is open will have undefined
//     results. Don’t do this. If you’re lucky SQLite will report the
//     database is corrupted, but it may also return incorrect or
//     garbage data.
//
// [shared-cache mode]: https://www.sqlite.org/sharedcache.html
type VFS struct {
	fileslock sync.Mutex
	files     map[string]Entry
}

// Entry adds a new file entry to the VFS. After doing this, it is
// generally unsafe to do anything which could change the data returned
// by that Entry. If you need to change the data, replace it with a
// different Entry (with some caveats; see [New]).
//
// Entry(name, nil) is equivalent to [VFS.Delete](name, false).
func (v *VFS) Entry(name string, r Entry) error {
	if r == nil {
		return v.Delete(name, false)
	}

	v.fileslock.Lock()
	if v.files == nil {
		v.files = make(map[string]Entry)
	}
	v.files[name] = r
	v.fileslock.Unlock()
	return nil
}

// Open implements sqlite3vfs.VFS.
//
// In the SQLite VFS model, files on immutable devices may still be
// opened “read-write”, and fail only when a write is attempted.
func (v *VFS) Open(name string, flags sqlite3vfs.OpenFlag) (sqlite3vfs.File, sqlite3vfs.OpenFlag, error) {
	v.fileslock.Lock()
	r := v.files[name]
	v.fileslock.Unlock()
	if r == nil {
		return nil, flags, sqlite3vfs.CantOpenError
	}
	return &file{r}, flags, nil
}

// Delete removes the entry from the VFS. This can be used to free
// resources you no longer need after closing all sql.Conn using the
// deleted file.
func (v *VFS) Delete(name string, dirSync bool) error {
	v.fileslock.Lock()
	delete(v.files, name)
	v.fileslock.Unlock()
	return nil
}

// Access implements sqlite3vfs.VFS.
func (v *VFS) Access(name string, flag sqlite3vfs.AccessFlag) (bool, error) {
	v.fileslock.Lock()
	r := v.files[name]
	v.fileslock.Unlock()
	return r != nil && flag != sqlite3vfs.AccessReadWrite, nil
}

// FullPathname implements sqlite3vfs.VFS.
func (*VFS) FullPathname(name string) string {
	return name
}

// Other than ReadAt and Size, the following is uninteresting and mostly
// unused boilerplate.

type file struct{ r Entry }

func (f file) ReadAt(b []byte, off int64) (n int, err error) {
	return f.r.ReadAt(b, off)
}

func (f file) FileSize() (int64, error) {
	return f.r.Size(), nil
}

func (file) Close() error {
	return nil
}

func (file) WriteAt(b []byte, off int64) (n int, err error) {
	return 0, sqlite3vfs.ReadOnlyError
}

func (file) Truncate(size int64) error {
	return sqlite3vfs.ReadOnlyError
}

func (file) Sync(flag sqlite3vfs.SyncType) error {
	return nil
}

func (file) Lock(elock sqlite3vfs.LockType) error {
	return nil
}

func (file) Unlock(elock sqlite3vfs.LockType) error {
	return nil
}

func (file) CheckReservedLock() (bool, error) {
	return false, nil
}

func (file) SectorSize() int64 {
	return 0
}

func (file) DeviceCharacteristics() sqlite3vfs.DeviceCharacteristic {
	return sqlite3vfs.IocapImmutable
}
