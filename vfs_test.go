package sqlite3memvfs

import (
	"bytes"
	"database/sql"
	_ "embed"
	"errors"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	_ "github.com/mattn/go-sqlite3"
	"github.com/psanford/sqlite3vfs"
)

//go:embed testdata/test.db
var testBytes []byte

var testVFS = Must(
	WithEntryData("bytes.db", testBytes),
	WithEntryData("string.db", string(testBytes)),
	RegisteredAs("testvfs"),
)

func TestOpenBytes(t *testing.T) {
	db, err := sql.Open("sqlite3", "bytes.db?vfs=testvfs")
	if err != nil {
		t.Fatal(err)
	}
	checkTestDB(t, db)
}

func TestOpenString(t *testing.T) {
	db, err := sql.Open("sqlite3", "string.db?vfs=testvfs")
	if err != nil {
		t.Fatal(err)
	}
	checkTestDB(t, db)
}

func TestAccess(t *testing.T) {
	// some things which I can’t figure out how to test via the
	// SQLite connection as the immutable device stops it from
	// trying to write at all.

	if ok, _ := testVFS.Access("bytes.db", sqlite3vfs.AccessExists); !ok {
		t.Error("bytes.db should exist")
	}
	if ok, _ := testVFS.Access("bytes.db", sqlite3vfs.AccessRead); !ok {
		t.Error("bytes.db should be readable")
	}
	if ok, _ := testVFS.Access("bytes.db", sqlite3vfs.AccessReadWrite); ok {
		t.Error("bytes.db should not be writable")
	}

	if ok, _ := testVFS.Access("missing.db", sqlite3vfs.AccessExists); ok {
		t.Error("missing.db should not exist")
	}
	if ok, _ := testVFS.Access("missing.db", sqlite3vfs.AccessRead); ok {
		t.Error("missing.db should not be readable")
	}
	if ok, _ := testVFS.Access("missing.db", sqlite3vfs.AccessReadWrite); ok {
		t.Error("missing.db should not be writable")
	}
}

func checkTestDB(t *testing.T, db *sql.DB) {
	defer func() {
		if err := db.Close(); err != nil {
			t.Error(err)
		}
	}()

	rows, err := db.Query("select * from test")
	if err != nil {
		t.Error(err)
		return
	}
	defer func() {
		if err := rows.Close(); err != nil {
			t.Error(err)
		}
	}()

	var id int
	var value string

	if got, err := rows.Columns(); err != nil {
		t.Error(err)
	} else if want := []string{"id", "value"}; !cmp.Equal(got, want) {
		t.Error(cmp.Diff(got, want))
	} else if !rows.Next() {
		t.Errorf("expected ID 1, but row exhausted: %s", rows.Err())

	} else if err := rows.Scan(&id, &value); err != nil {
		t.Errorf("expected to scan row 1 but: %s", rows.Err())
	} else if got, want := id, 1; got != want {
		t.Errorf("got %d, wanted %d", got, want)
	} else if got, want := value, "first row"; got != want {
		t.Errorf("got %q, wanted %q", got, want)

	} else if !rows.Next() {
		t.Errorf("expected ID 2, but row exhausted: %s", rows.Err())
	} else if err := rows.Scan(&id, &value); err != nil {
		t.Errorf("expected to scan row 2 but: %s", rows.Err())
	} else if got, want := id, 2; got != want {
		t.Errorf("got %d, wanted %d", got, want)
	} else if got, want := value, "second row"; got != want {
		t.Errorf("got %q, wanted %q", got, want)

	} else if rows.Next() {
		t.Error("expected rows to be exhausted after two items")
	}
}

func TestWrite(t *testing.T) {
	db, err := sql.Open("sqlite3", "bytes.db?vfs=testvfs")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := db.Close(); err != nil {
			t.Error(err)
		}
	}()
	if _, err := db.Exec("insert into test values (3, 'x')"); err == nil {
		t.Error("expected an error when inserting")
	} else if err.Error() != "attempt to write a readonly database" {
		t.Error("expected failure due to readonly, not", err)
	}
}

func TestOpenMissingFile(t *testing.T) {
	if db, err := sql.Open("sqlite3", "missing.db?vfs=testvfs"); err != nil {
		t.Error("expected Open to work, not", err)
	} else if err := db.Ping(); err == nil {
		t.Error("wanted ping to fail, but it succeeded")
	} else if !strings.Contains(err.Error(), "unable to open database file") {
		t.Error("expected failure due to missing file, not", err)
	} else {
		db.Close()
	}
}

func TestEntrySetDelete(t *testing.T) {
	db, err := sql.Open("sqlite3", "notyet.db?vfs=testvfs")
	if err != nil {
		t.Error("expected Open to work, not", err)
	}
	db.SetMaxIdleConns(0)
	if err := db.Ping(); err == nil {
		t.Error("wanted ping to fail, but it succeeded")
	} else if err := testVFS.Entry("notyet.db", bytes.NewReader(testBytes)); err != nil {
		t.Error("expected to add file entry, not", err)
	} else if err := db.Ping(); err != nil {
		t.Error("wanted ping to work now, but it failed with", err)
	} else if err := testVFS.Entry("notyet.db", nil); err != nil {
		t.Error("expected to delete file entry, not", err)
	} else if err := db.Ping(); err == nil {
		t.Error("wanted ping to fail, but it succeeded")
	} else if !strings.Contains(err.Error(), "unable to open database file") {
		t.Error("expected failure due to missing file, not", err)
	} else {
		db.Close()
	}
}

func TestOptionError(t *testing.T) {
	// No option I can find actually triggers an error yet, but make
	// sure the error handling path works regardless.
	want := errors.New("test error")
	defer func() {
		if got := recover(); got != want {
			t.Errorf("got %v, want %v", got, want)
		}
	}()
	Must(func(*VFS) error { return want })
}
